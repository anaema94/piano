/* $('.note').on('click', handleClick);

function handleClick() {

  let button = this.button;

  makeSound(button);

  buttonAnimation(button);
} */

for (var i = 0; i < document.querySelectorAll(".note").length; i++) {

    document.querySelectorAll(".note")[i].addEventListener("click", handleClick);

    function handleClick() {

        var buttonInnerHTML = this.innerHTML;

        makeSound(buttonInnerHTML);

        buttonAnimation(buttonInnerHTML);
    }

}

document.addEventListener("keydown", function() {

    makeSound(event.key);

    buttonAnimation(event.key);
});

function makeSound(key) {

    switch (key) {
        case "a":
            var cnote = new Audio("./sounds/do.mp3");
            cnote.play();
            break;

        case "s":
            var dnote = new Audio("./sounds/re.mp3");
            dnote.play();
            break;

        case "d":
            var enote = new Audio("sounds/mi.mp3");
            enote.play();
            break;

        case "f":
            var fnote = new Audio("./sounds/fa.mp3");
            fnote.play();
            break;

        case "g":
            var gnote = new Audio("sounds/sol.mp3");
            gnote.play();
            break;

        case "h":
            var anote = new Audio("./sounds/la.mp3");
            anote.play();
            break;

        case "j":
            var hnote = new Audio("sounds/si.mp3");
            hnote.play();
            break;

        case "k":
            var cnote2 = new Audio("sounds/do2.mp3");
            cnote2.play();
            break;

    }

}

function buttonAnimation(currentKey) {

    var activeButton = document.querySelector("." + currentKey);

    activeButton.classList.add("pressed");

    setTimeout(function() {
        activeButton.classList.remove("pressed");
    }, 100);

}